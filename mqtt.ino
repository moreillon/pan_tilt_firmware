void MQTT_setup(){
  Serial.println(F("[MQTT] MQTT setup"));
  MQTT_client.setServer(MQTT_BROKER_ADDRESS, MQTT_PORT);
  MQTT_client.setCallback(mqtt_message_callback);
}

boolean mqtt_connected(){
  return MQTT_client.connected();
}

String get_mqtt_base_topic(){
  String mqtt_username = read_string_from_eeprom(EEPROM_MQTT_USERNAME_ADDRESS);
  return "/" + mqtt_username + "/" + get_device_name();
}

String get_mqtt_status_topic(){
  return get_mqtt_base_topic() + "/status";
}

String get_mqtt_command_topic(){
  return get_mqtt_base_topic() + "/command";
}
String get_mqtt_username(){
  return read_string_from_eeprom(EEPROM_MQTT_USERNAME_ADDRESS);
}

String get_mqtt_password(){
  return read_string_from_eeprom(EEPROM_MQTT_PASSWORD_ADDRESS);
}

void MQTT_connection_manager(){

  static boolean last_connection_state = false;
  
  static long last_connection_attempt;
  
  if(mqtt_connected() != last_connection_state) {
    last_connection_state = mqtt_connected();

    if(mqtt_connected()){
      // Changed from disconnected to connected
      Serial.println(F("[MQTT] Connected"));
      
      Serial.println(F("[MQTT] Subscribing to topics"));
      MQTT_client.subscribe(get_mqtt_command_topic().c_str());

      mqtt_publish_state();
    }
    else {
      // Changed from connected to disconnected
      Serial.print(F("[MQTT] Disconnected: "));
      Serial.println(MQTT_client.state());

      
    }
  }

  // Kind of similar to the pubsubclient example, one connection attempt every 5 seconds
  if(!mqtt_connected()){
    if(millis() - last_connection_attempt > MQTT_RECONNECT_PERIOD){
      last_connection_attempt = millis();

      // No need to do anything if not connected to WiFi
      if(!wifi_connected()) return;
      
      Serial.println("[MQTT] Connecting");

      String mqtt_password = read_string_from_eeprom(EEPROM_MQTT_PASSWORD_ADDRESS);

      // Last will
      StaticJsonDocument<MQTT_MAX_PACKET_SIZE> outbound_JSON_message;
  
      outbound_JSON_message["connected"] = false;
      outbound_JSON_message["type"] = DEVICE_TYPE;
      outbound_JSON_message["nickname"] = get_device_nickname();
      outbound_JSON_message["firmware_version"] = DEVICE_FIRMWARE_VERSION;
      
      char mqtt_last_will[MQTT_MAX_PACKET_SIZE];
      serializeJson(outbound_JSON_message, mqtt_last_will, sizeof(mqtt_last_will));
      
      MQTT_client.connect(
        get_device_name().c_str(),
        get_mqtt_username().c_str(), 
        mqtt_password.c_str(),
        get_mqtt_status_topic().c_str(),
        MQTT_QOS,
        MQTT_RETAIN,
        mqtt_last_will
      );
    }
  }
  
}

void mqtt_message_callback(char* topic, byte* payload, unsigned int length) {
  // This function is called whenever a MQTT message arrives on a subscribed topic

  Serial.print("[MQTTT] message received on ");
  Serial.println(topic);

  // Create a JSON object to hold the message
  StaticJsonDocument<200> inbound_JSON_message;

  // Copy the message into the JSON object
  deserializeJson(inbound_JSON_message, payload);

  // Within the JSON object, the command is called "command"
  const char* command = inbound_JSON_message["command"];  

  if(!inbound_JSON_message.containsKey("command")) return;


  // Check what the command is and act accordingly
  if( strcmp(command, "up")==0 ) vertical.move(1);
  else if( strcmp(command, "down")==0 ) vertical.move(-1);
  else if( strcmp(command, "left")==0 ) horizontal.move(1);
  else if( strcmp(command, "right")==0 ) horizontal.move(-1);

}

void mqtt_publish_state(){
  Serial.println("[MQTT] State published");


  StaticJsonDocument<MQTT_MAX_PACKET_SIZE> outbound_JSON_message;
  
  outbound_JSON_message["connected"] = true;
  outbound_JSON_message["type"] = DEVICE_TYPE;
  outbound_JSON_message["nickname"] = get_device_nickname();
  outbound_JSON_message["firmware_version"] = DEVICE_FIRMWARE_VERSION;
  
  char mqtt_payload[MQTT_MAX_PACKET_SIZE];
  serializeJson(outbound_JSON_message, mqtt_payload, sizeof(mqtt_payload));
  MQTT_client.publish(get_mqtt_status_topic().c_str(), mqtt_payload, MQTT_RETAIN);
  
}
